﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Grooveshark.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        // Get: Login
        public ActionResult LogIn( )
        {
            Session["isLogged"] = 1;
            return RedirectToAction("Index","Home");
        }

        // GET: Login/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
    }
}
